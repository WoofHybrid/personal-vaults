# Personal Vaults
A simple but effective vaults plugin for your Minecraft servers.

## NOTE:
The original source code was lost for the first two versions of this plugin (available on [spigot](https://www.spigotmc.org/resources/personal-vaults.75159).)

This project is going to be split into three phases:
1. Recode the project (using the decompiled jar as a reference.)
2. Test and debug the project.
3. Add previously requested features.

After these three phases are finished, the plugin will be in a complete and usable state.

I will still be accepting suggestions and fixing any bugs you might encounter, just submit an issue to this repository with as much information as you can provide on the bug or suggestion.
