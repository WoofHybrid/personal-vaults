package com.woofhybrid.personalvaults;

import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

import java.util.logging.Logger;

class InventoryEvents implements Listener
{
    private InventoryTracker tracker;

    // Provides a way to get and set the tracker.
    InventoryTracker getTracker()
    {
        return tracker;
    }
    void setTracker(InventoryTracker tracker)
    {
        this.tracker = tracker;
    }

    @EventHandler
    private void onInventoryClosed(InventoryCloseEvent event)
    {
        // If the tracker has been set, pass the event to the tracker.
        if(tracker != null)
        {
            tracker.onVaultInventoryClosed(event);
        }
    }

    @EventHandler
    private void onInventoryClicked(InventoryClickEvent event)
    {
        // If the tracker has been set, pass the event to the tracker.
        if(tracker != null)
        {
            tracker.onVaultSlotClicked(event);
        }
    }
}
