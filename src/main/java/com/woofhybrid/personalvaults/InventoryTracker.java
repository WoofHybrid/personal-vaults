package com.woofhybrid.personalvaults;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

class InventoryTracker
{
    private Path dataPath;
    private int inventorySize;

    private ItemStack disabledSlot;

    private Logger logger;

    private LocalizedMessages messages;

    private List<PlayerVaultView> openVaults;

    private String vaultTitle;

    InventoryTracker(String dataFolderPath, Logger logger, int inventorySize, LocalizedMessages messages, ItemStack disabledSlot, String vaultTitle)
    {
        // Path to the folder where all data files are stored.
        dataPath = Paths.get(dataFolderPath, "playerdata");

        this.logger = logger;
        this.messages = messages;
        this.inventorySize = inventorySize;
        this.disabledSlot = disabledSlot;
        this.vaultTitle = vaultTitle;

        // Tracks open vaults so that events can be passed properly.
        openVaults = new ArrayList<>();

    }

    // Opens the player's vault when they run /pvault <vault-index>
    void openVault(Player player, int vaultIndex)
    {
        // Get players UUID, the file names use UUID to avoid issues with username changes.
        UUID playerId = player.getUniqueId();

        // Load player's vault data, and create a new view to track.
        PlayerVaultData pvd = new PlayerVaultData(playerId, logger, dataPath);
        PlayerVaultView pvw = new PlayerVaultView(disabledSlot, inventorySize, vaultIndex, playerId, pvd, vaultTitle);

        // Add the player's inventory view to the data view.
        InventoryView view = player.openInventory(pvw.getInventory());
        pvw.addViewer(view);
    }

    void closeAllVaults()
    {
        for(PlayerVaultView view : openVaults)
        {
            view.saveAllData();
            view.closeForAll();
        }

        openVaults.clear();
    }

    // Opens the vault of another player for an admin using /pvault see
    void openVaultFor(OfflinePlayer vaultOwner, int vaultIndex, Player targetViewer)
    {
        UUID playerId = vaultOwner.getUniqueId();

        for(PlayerVaultView view : openVaults)
        {
            if(view.getOwner().equals(playerId))
            {
                InventoryView vaultSeeInventory = targetViewer.openInventory(view.getInventory());
                view.addViewer(vaultSeeInventory);
                return;
            }
        }

        HashMap<String, String> placeholders = new HashMap<>();
        placeholders.put("player", vaultOwner.getName());
        placeholders.put("vault-index", Integer.toString(vaultIndex));

        PlayerVaultData pvd = new PlayerVaultData(playerId, logger, dataPath);
        if(pvd.getVault(vaultIndex) != null)
        {
            PlayerVaultView vaultView = new PlayerVaultView(disabledSlot, inventorySize, vaultIndex, playerId, pvd, vaultTitle);
            InventoryView view = targetViewer.openInventory(vaultView.getInventory());
            vaultView.addViewer(view);
            openVaults.add(vaultView);
        }
        else
        {
            String errorMessage = messages.getLocalizedMessage("vault-see-no-vault", targetViewer.getLocale(), placeholders);
            targetViewer.sendMessage(errorMessage);
        }
    }

    void onVaultInventoryClosed(InventoryCloseEvent event)
    {
        for(int i = 0; i < openVaults.size(); i++)
        {
            PlayerVaultView view = openVaults.get(i);
            if(view.getInventory() == event.getInventory())
            {
                view.removeViewer(event.getView());

                view.updateSavedContents();

                if(view.getViewerCount() <= 0)
                {
                    openVaults.remove(view);
                    view.saveAllData();
                }

                break;
            }
        }
    }

    void onVaultSlotClicked(InventoryClickEvent event)
    {
        // Go over all tracked vault views.
        for(int i = 0; i < openVaults.size(); i++)
        {
            // Get the vault view
            PlayerVaultView view = openVaults.get(i);

            // Check if the inventory in this event is associated with the vault view
            if(view.getInventory() == event.getInventory())
            {
                // If it is, check if the player is clicking on a disabled slot
                if(event.getSlot() >= inventorySize)
                {
                    // If they are, cancel the click (item won't be moved)
                    event.setCancelled(true);

                    // Tell the clicker's client that the item didn't move.
                    HumanEntity clicker = event.getWhoClicked();
                    if(clicker instanceof Player)
                    {
                        ((Player) clicker).updateInventory();
                    }
                }

                view.updateSavedContents();

                break;
            }
        }
    }
}
