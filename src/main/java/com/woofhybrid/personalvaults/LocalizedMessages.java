package com.woofhybrid.personalvaults;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

/**
 * A class that helps with getting translations in other languages.
 */
class LocalizedMessages
{
    // The FileConfiguration that holds the data from 'locale.yml'
    private FileConfiguration locale;

    // The default locale to use if the player's locale isn't found.
    private String defaultLocale;
    private Logger logger;

    /**
     * Creates an instance of LocalizedMessages that can be used to grab language specific messages.
     * @param locale The plugin instance providing the locale config.
     */
    LocalizedMessages(FileConfiguration locale, Logger logger)
    {
        this.logger = logger;
        Reload(locale);
    }

    void Reload(FileConfiguration locale)
    {
        // This will give us the localized messages.
        this.locale = locale;

        // If locale is null, complain about it to the log.
        if(locale == null)
        {
            logger.severe("locale config is null, all messages will be broken!");
        }
        else
        {
            // If locale isn't null, then grab the id of the fallback (default) locale.
            this.defaultLocale = locale.getString("default-locale");
        }
    }

    String getLocalizedMessage(String messageName, String lang, Map<String, String> placeholders)
    {
        // Attempt to grab the string the easy way.
        String r = locale.getString(lang + ".text." + messageName);

        // If the string wasn't found, search for a locale that might have the specified lang in its aliases
        if(r == null)
        {
            // If the language code is 4 chars, the first two chars should be just the language,
            // and not specific to the region. Look for that as well.
            String langShort = null;
            if(lang.length() > 2)
            {
                // Grab the first two characters
                langShort = lang.substring(0, 1);

                // Attempt to grab the string
                r = locale.getString(langShort + ".text." + messageName);

                // If the string was found
                if(r != null)
                {
                    // Return it.
                    return r;
                }
            }

            // Tracks if the locale was found.
            boolean wasFound;

            // Grab the name of all available locales.
            Set<String> allLanguageSections = locale.getKeys(false);

            // Go through every section and see if the name, or any if it's aliases match the provided ID.
            for(String langSec : allLanguageSections)
            {
                // Get a list of all the languages aliases.
                List<String> aliases = locale.getStringList(langSec + ".aliases");

                // Check if this language has either the specified lang (or the first 2 characters of the code, if set)
                wasFound = false;

                // Check if the specified locale is an alias.
                for(String alias : aliases)
                {
                    // First check the specified locale.
                    wasFound = alias.equalsIgnoreCase(lang);
                    if(!wasFound)
                    {
                        // If the specified locale isn't an alias, check the 2 character code if there is one.
                        if(langShort != null)
                        {
                            wasFound = alias.equalsIgnoreCase(langShort);
                        }
                    }
                }

                // If a locale containing the alias was found, no need to continue searching.
                if(wasFound)
                {
                    // Try to grab the string.
                    r = locale.getString(langSec + ".text." + messageName);
                    break;
                }
            }
        }

        // If the result wasn't found at all
        if(r == null)
        {
            // Try to grab the message using the default locale.
            r = locale.getString(defaultLocale + ".text." + messageName);

            // If the default locale doesn't have the message, then return the message name instead
            if(r == null)
            {
                r = messageName;
            }
        }

        // Chat colors with '&'
        r = ChatColor.translateAlternateColorCodes('&', r);

        // If placeholder values are provided
        if(placeholders != null)
        {
            // Replace all provided placeholders with their values.
            Set<Map.Entry<String, String>> placeholderSet = placeholders.entrySet();
            for (Map.Entry<String, String> placeholder : placeholderSet) {
                r = r.replace("%" + placeholder.getKey() + "%", placeholder.getValue());
            }
        }

        // Return the result.
        return r;
    }
}
