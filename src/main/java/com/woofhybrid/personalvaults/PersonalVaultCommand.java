package com.woofhybrid.personalvaults;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;

import java.util.*;

/**
 * The main command for this plugin.
 */
class PersonalVaultCommand implements CommandExecutor, TabCompleter
{
    // Change this if you want to increase, or reduce the maximum number of vaults a player can have.
    // Warning: Large values may cause lag!!
    private int maxVaultAmount = 50; // I might make this into a config option.

    // Various permissions that the player needs to do specific things with this command.
    private Permission useCommand; // Permission to use the command
    private Permission useCommandSee; // Permission to see other player's vaults.
    private Permission useCommandReload; // Permission to reload the plugin.

    // Searches the 'locale.yml' file for message translations in the player's locale
    private LocalizedMessages messages;
    private InventoryEvents eventHandler;

    private PersonalVaultsPlugin plugin;

    PersonalVaultCommand(LocalizedMessages messages, InventoryEvents eventHandler, PersonalVaultsPlugin plugin)
    {
        // Provides localization.
        this.messages = messages;

        // Manages opened GUIs
        this.eventHandler = eventHandler;

        // A reference to the plugin so the command can call the reload functions.
        this.plugin = plugin;

        // If you change any of these, it would be a good idea to also reflect these changes in the plugin.yml
        useCommand = new Permission("pvault.use", "Permission to use the '/pvault' command");
        useCommandSee = new Permission("pvault.admin.see", "Permission to use '/pvault see' to see other players' vaults");
        useCommandReload = new Permission("pvault.admin.reload", "Permission to use '/pvault reload' to reload the plugin's config.");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
    {
        // If the sender is null, it isn't a good idea to try to do anything here.
        if(sender == null) return false;

        // Get the tracker from the event handler
        InventoryTracker tracker = eventHandler.getTracker();

        // The locale to look for translated messages in
        String locale = "none";

        HashMap<String, String> placeholders = new HashMap<>();

        // Check if the sender is a player.
        Player playerSender = null;
        if(sender instanceof Player)
        {
            // If the sender is a player, cast and store the player instance for later use.
            playerSender = (Player)sender;

            // As far as I'm aware, Player is the only class that has a way to get a locale,
            // and also implements CommandSender.
            locale = playerSender.getLocale();
        }

        // Tracks if the permission error should be shown
        boolean noPermission = true;

        // Does the sender have permission to use this command?
        if(sender.hasPermission(useCommand))
        {
            // If so, don't show the permission error (yet)
            noPermission = false;

            // If there aren't any arguments provided
            if(args.length <= 0)
            {
                // Tell the user that they didn't specify any arguments.
                String errorMessage = messages.getLocalizedMessage("command-no-arguments", locale, null);
                sender.sendMessage(errorMessage);
            }
            else
            {
                // If at least one argument has been provided.

                // Grab the first argument.
                String arg0 = args[0].toLowerCase();

                // Do what the player says (if they have the right permissions)
                switch(arg0)
                {
                    // For the command '/pvault'
                    default: // It isn't any of the below ones, so check if it's a number.
                        if(tracker == null)
                        {
                            // If the tracker doesn't exist, don't attempt to perform the command.
                            String errorMessage = messages.getLocalizedMessage("command-config-error", locale, null);
                            sender.sendMessage(errorMessage);
                        }
                        else
                        {
                            try
                            {

                                // If the argument is a number, make sure the sender is a player.
                                // Only players can open vaults.
                                if (playerSender == null)
                                {
                                    // If they aren't a player
                                    sender.sendMessage("Only players can access vaults.");
                                }
                                else
                                {
                                    int vaultNum;
                                    vaultNum = Integer.parseInt(arg0);

                                    placeholders.put("vault-index", Integer.toString(vaultNum));

                                    // check if they have permission to open the specified vault.

                                    int permissionVault = vaultNum;
                                    boolean hasPermission = false;

                                    // If the player specifies a negative vault number, or 0
                                    if (vaultNum <= 0)
                                    {

                                        // Tell them this isn't allowed
                                        String errorMessage = messages.getLocalizedMessage("vault-negative-index", locale, placeholders);
                                        sender.sendMessage(errorMessage);
                                    }

                                    // Don't do any costly checks if the player specifies a vault grater than the max amount
                                    if (vaultNum <= plugin.getMaxVaultAmount())
                                    {
                                        // The purpose of this code is to make sure the player can open this vault.
                                        // It checks if player can open the same amount of vaults as the vault number they
                                        // provided or if they can open more vaults than the one provided.
                                        while (permissionVault < plugin.getMaxVaultAmount())
                                        {
                                            // The permission to check for.
                                            String permission = "pvault.count." + permissionVault;

                                            // Check for it.
                                            hasPermission = sender.hasPermission(permission);


                                            // If they have it, then we don't need to check for any higher permissions.
                                            if (hasPermission) break;

                                            // If they don't have it, check for the next permission higher.
                                            permissionVault++;
                                        }
                                    }

                                    // If the player has permission to open this vault
                                    if (hasPermission)
                                    {
                                        // Open the GUI for them.
                                        tracker.openVault(playerSender, vaultNum);
                                    }
                                    else
                                    {
                                        // Tell the player that they can't access this vault
                                        String errorMessage = messages.getLocalizedMessage("vault-no-permission", locale, placeholders);
                                        sender.sendMessage(errorMessage);
                                    }
                                }
                            }
                            catch (NumberFormatException nfEx)
                            {
                                // The argument wasn't a number.
                                // Tell the user that they didn't send valid command.
                                String errorMessage = messages.getLocalizedMessage("invalid-command", locale, null);
                                sender.sendMessage(errorMessage);
                            }
                        }
                        break;

                        // for the command /pvault see
                    case "see":
                        if(tracker == null)
                        {
                            // If the tracker doesn't exist, don't attempt to perform the command.
                            String errorMessage = messages.getLocalizedMessage("command-config-error", locale, null);
                            sender.sendMessage(errorMessage);
                        }
                        else
                        {
                            if (playerSender == null)
                            {
                                // If they aren't a player
                                sender.sendMessage("Only players can access vaults.");
                            }

                            // If the player has permission to use /pvault see
                            if (sender.hasPermission(useCommandSee))
                            {
                                if (args.length < 3)
                                {
                                    // If the sender doesn't have permission to use this command, send an error message.
                                    String errorMessage = messages.getLocalizedMessage("vault-see-no-arguments", locale, null);
                                    sender.sendMessage(errorMessage);
                                }
                                else
                                {
                                    String userIdentifiable = args[1];

                                    OfflinePlayer target = Bukkit.getPlayer(userIdentifiable);
                                    if (target == null)
                                    {
                                        target = Bukkit.getPlayer(UUID.fromString(userIdentifiable));

                                        if (target == null)
                                        {
                                            OfflinePlayer[] offlinePlayers = Bukkit.getOfflinePlayers();

                                            for (OfflinePlayer plr : offlinePlayers)
                                            {
                                                if (plr.getName().equalsIgnoreCase(userIdentifiable))
                                                {
                                                    target = plr;
                                                    break;
                                                }

                                                if (plr.getUniqueId().equals(UUID.fromString(userIdentifiable)))
                                                {
                                                    target = plr;
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                    if (target == null)
                                    {
                                        placeholders.put("player", userIdentifiable);

                                        // If the player doesn't exist
                                        String errorMessage = messages.getLocalizedMessage("vault-see-no-player", locale, null);
                                        sender.sendMessage(errorMessage);
                                    }
                                    else
                                    {
                                        placeholders.put("player", target.getName());

                                        try
                                        {
                                            int vaultNum = Integer.parseInt(args[2]);

                                            // Open the vault for the admin
                                            tracker.openVaultFor(target, vaultNum, playerSender);
                                        }
                                        catch (NumberFormatException nfEx)
                                        {
                                            // The specified vault index isn't a valid number.
                                            // Tell the user that they didn't send valid command.
                                            String errorMessage = messages.getLocalizedMessage("invalid-command", locale, null);
                                            sender.sendMessage(errorMessage);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                // Show the permission error.
                                noPermission = true;
                            }
                        }
                        break;

                        // For the command /pvault reload
                    case "reload":
                        if(sender.hasPermission(useCommandReload))
                        {
                            sender.sendMessage("\u00a7eReloading...");
                            plugin.reloadFileConfig();
                        }
                        break;
                }
            }
        }

        if(noPermission)
        {
            // If the sender doesn't have permission to use this command, send an error message.
            String errorMessage = messages.getLocalizedMessage("command-no-use-permission", locale, null);
            sender.sendMessage(errorMessage);
        }

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {

        // The only sub-command that needs tab completion is the '/pvault see' sub-command
        if(sender.hasPermission(useCommandSee))
        {
            if(args.length >= 1)
            {
                if(args[0].equalsIgnoreCase("see"))
                {
                    if(args.length == 1)
                    {
                        List<String> tabComplete = new ArrayList<>();

                        // Suggesting player names
                        Collection<? extends Player> players = Bukkit.getOnlinePlayers();
                        for(Player p : players)
                        {
                            tabComplete.add(p.getName());
                        }

                        return tabComplete;
                    }
                }
            }
        }

        return null;
    }
}
