package com.woofhybrid.personalvaults;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * The main class of the Personal Vaults plugin.
 */
public class PersonalVaultsPlugin extends JavaPlugin
{
    // Logger for the plugin
    private Logger logger;

    // FileConfiguration for 'config.yml'
    private FileConfiguration config;

    // FileConfiguration for 'locale.yml'
    private FileConfiguration locale;

    // Tracker which processes inventory events associated with vaults.
    private InventoryTracker tracker;

    // Provides a way to get translated messages.
    private LocalizedMessages messages;

    // Event handler for inventory events
    private InventoryEvents inventoryEvents;

    // The pvault command
    private PersonalVaultCommand command;

    private int maxVaultAmount = 50;

    int getMaxVaultAmount()
    {
        return maxVaultAmount;
    }

    /**
     * Handles the general initialization of the plugin.
     */
    @Override
    public void onEnable()
    {
        // Get the logger
        logger = getLogger();

        // Initialize the event handler and locale manager
        inventoryEvents = new InventoryEvents();

        // Create the command
        command = new PersonalVaultCommand(messages, inventoryEvents, this);

        // Register the command
        Bukkit.getPluginCommand("pvault").setExecutor(command);
        Bukkit.getPluginCommand("pvault").setTabCompleter(command);

        // Load the configs
        reloadFileConfig();

        // Initialize the messages
        messages = new LocalizedMessages(locale, logger);
    }

    /**
     * Creates config folder and config files if they don't exist, and then reloads the config.
     */
    void reloadFileConfig()
    {
        if(tracker != null)
        {
            // If a tracker already exists (meaning, the plugin is reloading), close all open vaults to prevent glitches.
            tracker.closeAllVaults();
        }


        // Get the folder that this plugin should use to store its configs, and various other data.
        File dataFolder = getDataFolder();

        // Does the data folder exist? If so, is it actually a directory?
        if(!dataFolder.isDirectory())
        {
            // If not, create the data folder.
            dataFolder.mkdirs();
        }

        // Path to the 'config.yml' file
        File configYml = Paths.get(dataFolder.getPath(), "config.yml").toFile();

        // Does the data folder have a 'config.yml'?
        if(!configYml.isFile())
        {
            // If not, save the default config.yml
            saveEmbeddedFile("config.yml", configYml);

            // Note: I know that saveDefaultConfig(), and reloadConfig() exist.
            // However, it seems safer to do this than assume the plugin can't break if Spigot decides to change the
            // way getDataFolder(), saveDefaultConfig() or reloadConfig() work.
        }

        // Try loading the config.yml file.
        config = loadFileConfig(config, configYml);


        // Path to the 'locale.yml' file
        File localeYml = Paths.get(dataFolder.getPath(), "locale.yml").toFile();

        // Does the data folder have a 'locale.yml'?
        if(!localeYml.isFile())
        {
            // If not, save the default locale.yml
            saveEmbeddedFile("locale.yml", configYml);
        }

        // Try loading the locale.yml file
        locale = loadFileConfig(locale, localeYml);

        // Create a new LocalizedMessages instance with the updated stuff.
        messages = new LocalizedMessages(locale, logger);

        // Get the size of the vaults
        int invSize = config.getInt("inventory-size", 27);
        String vaultTitle = config.getString("vault-title", "&3&l%player%'s vault %vault-index%");

        ItemStack disabledSlotItem = null;
        // Create the disabled slot itemstack
        if(config.isConfigurationSection("disabled-slot-item"))
        {
            // Get the material id stored in the config
            String dsMatStr = config.getString("disabled-slot-item.material");

            // Clean the material id for use with the bukkit API
            String cleanedDsMatStr = dsMatStr.replace(' ', '_').replace('-', '_');
            cleanedDsMatStr = cleanedDsMatStr.toUpperCase();

            // Attempt to get the material with the specified ID
            Material mat = Material.getMaterial(cleanedDsMatStr);

            if(mat == null)
            {
                // If the material wasn't found, complain to the console.
                logger.severe("Unable to create disabled-slot-item, unknown material '" + dsMatStr + "' (Converted to " + cleanedDsMatStr + ")");
            }
            else
            {
                // Create a new itemstack with the specified material
                disabledSlotItem = new ItemStack(mat);

                // Set the amount according to the config.
                int amount = config.getInt("disabled-slot-item.amount", 1);
                disabledSlotItem.setAmount(amount);

                ItemMeta disabledItemMeta = disabledSlotItem.getItemMeta();

                // Get and colorize display name
                String displayedName = config.getString("disabled-slot-item.name");
                displayedName = ChatColor.translateAlternateColorCodes('&', displayedName);

                // Get and colorize lore
                List<String> lore = config.getStringList("disabled-slot-item.lore");

                // With every line of lore
                for(int i = 0; i < lore.size(); i++)
                {
                    // Get the line.
                    String loreString = lore.get(i);
                    // Colorize the line.
                    loreString = ChatColor.translateAlternateColorCodes('&', loreString);
                    // Set the line to the colorized version.
                    lore.set(i, loreString);
                }

                // Apply display name
                disabledItemMeta.setDisplayName(displayedName);

                // Apply lore
                disabledItemMeta.setLore(lore);

                // If the item should have an enchantment glint
                boolean hasGlint = config.getBoolean("disabled-slot-item.glowing", false);
                if(hasGlint)
                {
                    // Disable the enchantment list that's normally shown when hovering over the item.
                    disabledItemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);

                    // Apply a dummy enchantment, this is why showing the enchantment list is disabled above.
                    disabledItemMeta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true);
                }
            }
        }

        // Create a new tracker with the updated stuff.
        tracker = new InventoryTracker(getDataFolder().getPath(), logger, invSize, messages, disabledSlotItem, vaultTitle);

        // Set the event handler to use the new tracker.
        inventoryEvents.setTracker(tracker);

        // The maximum number of vaults a player can have
        maxVaultAmount = config.getInt("max-vault-amount", 50);
    }

    /**
     * Copies an embedded file to the specified output file.
     * @param embeddedFile
     * @param outputFile
     */
    private void saveEmbeddedFile(String embeddedFile, File outputFile)
    {
        // Defining these outside of the try-catch, so that if something DOES go wrong, I can still close these
        // to prevent memory leaking.
        InputStream embeddedFileStream = null;
        FileOutputStream outFileStream = null;

        try
        {
            embeddedFileStream = getResource(embeddedFile);

            // If not, then create the file
            outputFile.createNewFile();

            // Open the newly created file, to write the embedded data.
            outFileStream = new FileOutputStream(outputFile);

            // Copy the data from the embedded stream, to the new file.
            embeddedFileStream.transferTo(outFileStream);

            // Make sure all the data is written before closing.
            outFileStream.flush();

            // Close the output stream, it's no longer needed.
            outFileStream.close();
        }
        catch(IOException ioEx1)
        {
            logger.severe("An exception occurred while attempting to write to the file: " + outputFile.getName());
            ioEx1.printStackTrace();
        }

        // The embedded file's stream is no longer needed.
        try
        {
            // If it's not null (it has been opened)
            if (embeddedFileStream != null)
            {
                // Close it.
                embeddedFileStream.close();
            }
        }
        catch(IOException ioEx2)
        {
            // If an issue occurred, it's most likely that
            // the embeddedFileStream is somehow not closable.
        }

        // If everything worked correctly, the output file shouldn't be open.
        // If it is, close it so that this isn't a possible memory leak.
        try
        {
            // If it's not null (it has been opened)
            if (outFileStream != null)
            {
                // Close it.
                outFileStream.close();
            }
        }
        catch(IOException ioEx2)
        {
            // If there was an error, log it to the console.
            logger.severe("Unable to close embeddedFileStream");
            ioEx2.printStackTrace();
        }
    }

    // Loads a yml file with some additional safety checks.
    private FileConfiguration loadFileConfig(FileConfiguration config, File file)
    {
        FileConfiguration out;

        if(config != null)
        {
            out = config;
        }
        else
        {
            out = new YamlConfiguration();
        }

        try
        {
            // Try loading the specified file.
            out.load(file);
        }
        catch(FileNotFoundException fnfEx)
        {
            // If the file doesn't exist, log the issue.
            logger.severe("Unable to load the file: " + file.getName() + ", file was not found.");
        }
        catch(IOException ioEx3)
        {
            // If an exception occurred while loading the file.
            logger.severe("Unable to read file: " + file.getName());
            ioEx3.printStackTrace();
        }
        catch(InvalidConfigurationException icEx)
        {
            // If the file isn't a valid YML file, then log the issue.
            // (The most common issue is using tabs instead of spaces,
            // so hopefully suggesting this to the user will save some time.)
            logger.severe("Unable to load " + file.getName() + ", invalid YML file. " +
                    "(Make sure the file doesn't use tabs for indentation.)");

            // The stacktrace might also contain some useful info in this situation.
            icEx.printStackTrace();
        }

        // Return the (hopefully) loaded file configuration.
        return out;
    }
}
