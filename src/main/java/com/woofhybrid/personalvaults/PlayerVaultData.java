package com.woofhybrid.personalvaults;

import java.io.*;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Logger;

class PlayerVaultData implements Serializable
{
    // The owner of these vaults (transient because the UUID is implied by the filename when it's stored.)
    private transient UUID owner;

    // The path to the directory where player vault files are stored.
    private transient Path storageLocation;

    // Contains all the inventories, and all of their items.
    private Map<Integer, PlayerVaultStorage> vaults;

    // Logger for errors and stuff.
    private Logger logger;

    // Format version, helpful if the format ever changes.
    // There are no data fixers yet, sense there aren't really any format changes.
    // Updating the data files from plugin versions before 2.0 will require the use of a jar to do this.
    private int formatVersion;

    // If the format ever changes, a data fixer can update to the new format.
    private transient final int CurrentFormatVersion = 0;

    PlayerVaultData(UUID owner, Logger logger, Path storageLocation)
    {
        this.owner = owner;
        this.logger = logger;
        this.storageLocation = storageLocation;

        // Default the format version of the data to the current version.
        formatVersion = CurrentFormatVersion;

        // Load data, or initialize before use.
        load();
    }

    void load()
    {
        // Name of the data file
        String dataFileString = owner + ".dat";

        // Get the data file
        File f = storageLocation.resolve(dataFileString).toFile();

        // Does the file exist, and is it actually a file?
        if(f.isFile())
        {
            try
            {
                // Open the file to load the data
                FileInputStream dataInputStream = new FileInputStream(f);

                // Used to deserialize java objects
                ObjectInputStream objectInputStream = new ObjectInputStream(dataInputStream);

                // Read the first object from the file, this SHOULD be the map of vault inventories.
                Object data = objectInputStream.readObject();

                // Check if it's actually a map.
                if(data instanceof Map)
                {
                    // Try to cast it to our vault inventory map.
                    vaults = (Map<Integer, PlayerVaultStorage>)data;
                }
            }
            catch(FileNotFoundException fnfEx)
            {
                // If the file wasn't found,
                // even though the if statement surrounding this code said the file DOES exist.
                logger.warning("Data file '" + dataFileString + "', was not found");
                fnfEx.printStackTrace();
            }
            catch(ClassNotFoundException cnfEx)
            {
                // If the class stored in the file wasn't found.
                logger.severe("Unable to find the class associated with '" + dataFileString + "'. " +
                        "This is most likely a data corruption issue, or the file / plugin is outdated.");
                cnfEx.printStackTrace();
            }
            catch(IOException ioEx)
            {
                // If there was an issue reading from the file.
                logger.severe("Failed to read from data file '" + dataFileString + "'.");
                ioEx.printStackTrace();
            }
            catch(ClassCastException ccEx)
            {
                // If the data is actually a map, but it isn't a "Map<Integer, PlayerVaultStorage>"
                logger.severe("Data from file '" + dataFileString + "' is most likely corrupted, or unsupported.");
                ccEx.printStackTrace();
            }
        }

        // If the vault inventories failed to load, or the file wasn't found
        if(vaults == null)
        {
            // Create a new map of vault inventories.
            vaults = new HashMap<>();
        }
        else
        {
            // If the vault inventories successfully loaded.

            // Get the created/used vaults
            Set<Integer> vaultIndices = vaults.keySet();

            // Go over all created/used vaults
            for(Integer vaultIndex : vaultIndices)
            {
                // Get the storage associated with this vault
                PlayerVaultStorage vaultData = vaults.get(vaultIndex);

                // Update the transient vars
                vaultData.updateTransientVars();
            }
        }
    }

    void save()
    {
        // Name of the data file
        String dataFileString = owner + ".dat";

        // Get the data file
        File f = storageLocation.resolve(dataFileString).toFile();

        // If the file doesn't exist.
        if(!f.exists())
        {
            try
            {
                // Attempt to create it.
                f.createNewFile();
            }
            catch(IOException ioEx)
            {
                // If an error occurred while creating the file, report it to the console.
                logger.severe("Unable to create data file '" + dataFileString + "'.");
                ioEx.printStackTrace();

                // Don't try to execute the rest of the function, sense the file wasn't created.
                return;
            }
        }

        try
        {
            // Open the file for writing
            FileOutputStream dataOutputStream = new FileOutputStream(f);

            // Used to write java objects
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(dataOutputStream);

            // Write the inventory map
            objectOutputStream.writeObject(vaults);

            // Make doubly sure the data was written before closing.
            objectOutputStream.flush();

            // Close the file, the data has been written to it.
            dataOutputStream.close();
        }
        catch (FileNotFoundException e)
        {
            // If the file wasn't found, even though the checks above check for that.
            logger.severe("Data file '" + dataFileString + "', was not found.");
            e.printStackTrace();
        }
        catch (IOException e)
        {
            logger.severe("Failed to write to data file '" + dataFileString + "'.");
            e.printStackTrace();
        }
    }

    PlayerVaultStorage getVault(int index)
    {
        // Returns the vault inventory, or null if it doesn't exist.
        return vaults.getOrDefault(index, null);
    }

    void setVault(int index, PlayerVaultStorage vault)
    {
        // If the vault is null, don't save it.
        if(vault == null)
        {
            vaults.remove(index);
            return;
        }

        // Don't save the vault if it's empty. This is to conserve disk space.
        if(vault.getItemCount() <= 0)
        {
            vaults.remove(index);
            return;
        }

        // If it's not empty, then we can save it.
        vaults.put(index, vault);
    }
}
