package com.woofhybrid.personalvaults;

import org.bukkit.inventory.ItemStack;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

class PlayerVaultStorage implements Serializable
{
    private HashMap<Integer, ItemStack> items;
    private transient int itemCount;

    int getItemCount()
    {
        return itemCount;
    }

    void setItem(int slot, ItemStack stack)
    {
        if(stack == null)
        {
            removeItem(slot);
        }
        else items.put(slot, stack);
    }

    void removeItem(int slot)
    {
        if(items.containsKey(slot))
        {
            items.remove(slot);
        }
    }

    ItemStack getItem(int slot)
    {
        return items.get(slot);
    }

    PlayerVaultStorage()
    {
        items = new HashMap<>();
        itemCount = 0;
    }

    void updateTransientVars()
    {
        // Reset all transient vars
        itemCount = 0;

        // Grab all used slot indices
        Set<Integer> keys = items.keySet();

        // Go over every slot
        for(Integer slot : keys)
        {
            // Only do stuff if the slot actually exists. (This is very redundant, but it's still safer)
            if(items.containsKey(slot))
            {
                // Get the items in that slot
                ItemStack val = items.get(slot);

                // If the slot is null (empty)
                if (val == null)
                {
                    // Remove this slot, saves (very slightly) on disk space and memory.
                    items.remove(slot);
                }

                // Add the stack's count to the item count
                itemCount += val.getAmount();
            }
        }
    }
}
