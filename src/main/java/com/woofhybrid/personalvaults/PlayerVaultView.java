package com.woofhybrid.personalvaults;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Manages a GUI associated with a currently opened vault.
 */
class PlayerVaultView
{
    private ItemStack disabledSlot;
    private int inventorySize;
    private int vaultIndex;
    private UUID owner;
    private PlayerVaultData data;
    private List<InventoryView> views;
    private Inventory inventory;

    PlayerVaultView(ItemStack disabledSlot, int inventorySize, int vaultIndex, UUID owner,
                    PlayerVaultData data, String vaultTitle)
    {
        this.disabledSlot = disabledSlot;
        this.inventorySize = inventorySize;
        this.vaultIndex = vaultIndex;
        this.owner = owner;
        this.data = data;

        // Get the inventory size as a multiple of nine.
        int invSize = inventorySize + (inventorySize % 9);

        views = new ArrayList<>();

        // Replace placeholders and apply coloring
        vaultTitle = ChatColor.translateAlternateColorCodes('&', vaultTitle);
        vaultTitle = vaultTitle
                .replace("%player%", Bukkit.getOfflinePlayer(owner).getName())
                .replace("%vault-index%", Integer.toString(vaultIndex));

        inventory = Bukkit.createInventory(null, invSize, vaultTitle);
    }

    Inventory getInventory()
    {
        return inventory;
    }

    UUID getOwner()
    {
        return owner;
    }

    int getVaultIndex()
    {
        return vaultIndex;
    }

    int getInventorySize()
    {
        return inventorySize;
    }

    void addViewer(InventoryView view)
    {
        views.add(view);
    }

    void removeViewer(InventoryView view)
    {
        views.remove(view);
    }

    int getViewerCount()
    {
        return views.size();
    }

    void saveAllData()
    {
        data.save();
    }

    void closeForAll()
    {
        for(InventoryView view : views)
        {
            view.close();
        }
    }

    void updateSavedContents()
    {
        // Get any storage that might have been previously saved for this view's vault index
        PlayerVaultStorage storage = data.getVault(vaultIndex);

        // If there isn't an already saved storage
        if(storage == null)
        {
            // Create a new storage to store whatever might be in this inventory
            storage = new PlayerVaultStorage();
        }

        // Go over all usable inventory slots
        for(int i = 0; i < inventorySize; i++)
        {
            // Get the stack in that slot
            ItemStack stack = inventory.getItem(i);

            // If there was no stack
            if(stack == null)
            {
                // Remove any items that might have been previously saved here
                storage.removeItem(i);
            }
            else
            {
                // If there is a stack, but it's amount is 0
                if(stack.getAmount() == 0)
                {
                    // Remove any items that might have been previously saved here
                    storage.removeItem(i);
                }

                // Otherwise, set the storage slot to contain this stack.
                storage.setItem(i, stack);
            }
        }

        // Needed to get current item count
        storage.updateTransientVars();

        // Check if the storage has items
        if(storage.getItemCount() > 0)
        {
            // If so, then assign the storage to this view's vault index.
            data.setVault(vaultIndex, storage);
        }
        else
        {
            // If not, remove any existing storage from this vault index.
            data.setVault(vaultIndex, null);
        }
    }
}
